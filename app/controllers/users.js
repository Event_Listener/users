'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const { wrap: async } = require('co');
const { respond } = require('../utils');
const User = mongoose.model('User');

/**
 * Load
 */

exports.load = async(function* (req, res, next, _id) {
  const criteria = {_id };
  try {
    req.profile = yield User.load({criteria});
    if (!req.profile) return next(new Error('User not found'));
    res.json(req.profile);
  } catch (err) {
    return next(err);
  }
  next();
});

/**
 * Create user
 */

exports.create = async(function* (req, res) {
  const user = new User(req.body);
  user.provider = 'local';
  try {
    yield user.save();
    req.profile = user;
    res.json(req.profile);
  } catch (err) {
    const errors = Object.keys(err.errors)
      .map(field => err.errors[field].message);
    res.json(req.profile);
  }
});

/**
 *  Show profile
 */

exports.show = function (req, res) {
  const user = req.profile;
  respond(res, 'users/show', {
    title: user.name,
    user: user
  });
};