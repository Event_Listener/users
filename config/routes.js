'use strict';

/**
 * Module dependencies.
 */

const users = require('../app/controllers/users');

module.exports = function (app) {
	app.get('/users', users.load);
	app.get('/users/:userId', users.show);
	app.post('/users', users.create);
	//app.put('/users/:userId', users.update);
	app.param('userId', users.load);

	app.use(function (err, req, res, next) {
		res.status(500)
		res.render('error', { error: err })
	})
}