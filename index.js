const fs = require('fs');
const express = require('express');
const mongoose = require('mongoose');
const join = require('path').join;
const models = join(__dirname, 'app/models');
const app = express();

module.exports = app;

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.search(/^[^\.].*\.js$/))
  .forEach(file => require(join(models, file)));

function connect () {
	return mongoose.connect('mongodb://localhost/test').connection;
}

function listen () {
	if (app.get('env') === 'test') return;
	app.listen(3000);
	}

connect()
	.on('error', console.log)
	.on('disconnected', connect)
	.once('open', listen);
require('./config/routes')(app);
require('./config/express')(app);
